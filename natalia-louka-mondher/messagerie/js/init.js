import dataBuilder from "./databuilder.js";
import eventsManager from "./eventsmanager.js";

const $main = document.querySelector('main');
//on récupère l'élément html qui contient le texte brut des pads fragments
const $dataFragments = $main.querySelector('#fragments');
//on récupère l'élément html qui contient le texte brut du pad index
const $dataIndex = $main.querySelector('#index');

//on construit un objet "data" à partir de ces deux éléments html
//cet objet contient les données structurées et différentes fonctions utiles pour y accéder
const data = dataBuilder($dataFragments, $dataIndex);
const events = eventsManager($main);

//les données sont chargées; on vide la page de son contenu 
$main.innerHTML = '';

const fragment = data.getRandomFragment();
const idFragment = fragment.id;
const contentFragment = fragment.html;
const idParents = data.getParentsFragments(idFragment)[0];
const idChildren = data.getChildrenFragments(idFragment)[0];
const reference = data.getReference(idFragment);
const user = reference.from;
console.log(user)

// on commence à structurer notre page HTML
const article = document.createElement("article");
$main.appendChild(article);

// si le fragment à un ou plusieurs parent(s) alors on le(s) affiche dans la page
if (idParents){
    const sectionParents = document.createElement("section");
    sectionParents.classList.add("parents");
    article.appendChild(sectionParents);
    idParents.forEach(idParent => {
        const referenceParent = data.getReference(idParent);
        const userParent = referenceParent.from;
        const divParent = document.createElement("div");
        divParent.classList.add("parent");
        divParent.setAttribute('data-id', idParent);
        sectionParents.appendChild(divParent);
        const contentParent = data.getFragment(idParent).html;
        divParent.innerHTML =  userParent + " a envoyé une message précèdemment";
    });
}

// on intègre le fragment dans la page
const section = document.createElement("section");
const div = document.createElement("div");
section.classList.add("fragment");
div.setAttribute('data-id', idFragment);
article.appendChild(section);
section.appendChild(div);
div.innerHTML = user + " vient d'écrire un message";

// si le fragment à un ou plusieurs enfant(s) alors on le(s) affiche dans la page
if (idChildren){
    const sectionChildren = document.createElement("section");
    sectionChildren.classList.add("children");
    article.appendChild(sectionChildren);
    idChildren.forEach(idChild => {
        const referenceChild = data.getReference(idChild);
        const userChild = referenceChild.from;
        const divChild = document.createElement("p");
        const spanChild = document.createElement("span");
        // divChild.classList.add("child");
        // divChild.setAttribute('data-id', idChild);
        // divChild.appendChild(spanChild);
        divChild.innerHTML = " veut écrire un message";
        spanChild.innerHTML = userChild;
        divChild.insertAdjacentHTML('afterbegin', spanChild);
        
        sectionChildren.appendChild(divChild);
        
        
        // const contentChild = data.getFragment(idChild).html;
        
        
    });
}

// // si je click sur un parent alors il devient fragment
// const parents = $main.querySelectorAll(".parent");
// if (parents){
//     parents.forEach(parent => {
//         parent.addEventListener("click", function(){
//             const newId = parent.getAttribute("data-id");
//             // on ajoute une ancre à notre page web en précisant l'id du nouveau fragment
//             window.location.hash = newId;
//             // on refresh la page
//             window.location.reload(true);
//         }); 
//     });
// }

// // si je click sur un child alors il devient fragment
// const children = $main.querySelectorAll(".child");
// if (children){
//     children.forEach(child => {
//         child.addEventListener("click", function(){
//             const newId = child.getAttribute("data-id");
//             // on ajoute une ancre à notre page web en précisant l'id du nouveau fragment
//             window.location.hash = newId;
//             // on refresh la page
//             window.location.reload(true);
//         }); 
//     });
// }