import { Previewer } from "./libs/paged.esm.js";

const eventsManager = ($main) => {


    //convertit en page.js
    const doPage = () => {
        const paged = new Previewer();
        const content = $main.innerHTML;
        $main.innerHTML = '';
        const flow = paged.preview(content, ["css/paged.css"], $main).then((flow) => {
	        console.log("Rendered", flow.total, "pages.");
        });

    };

    const initEvents = () => {
        document.addEventListener('keydown', (e) => {
            
            switch(e.key){
                case 'p':
                    doPage();
                    return;
                

            }
            console.log(e);
        });
    };

    initEvents();

};

export default eventsManager;