import mdToHtml from "./mdconverter.js";

const dataBuilder = ($dataFragments, $dataRef) => {

    const fragments = {};
    const references = {};




    //TOOLBOX: la boîte à outil qui permet de récupérer des données. Chaque fonction définie ici doit être
    //aussi déclarée comme valeur de retour, tout à la fin de ce fichier: voir commentaire "RETOUR"
    const getRandomFragment = () => {
        const keys = Object.keys(fragments);
        const randomKey = keys[Math.floor(Math.random() * keys.length)];
        return getFragment(randomKey);
    };

    const getFragment = (id) => {
        return fragments[id];
    };

    const getReference = (id) => {
        if(references[id])
            return references[id];
        return null;
    };

    const getLinkedFragments = (id) => {
        return [fragments[id].parents, fragments[id].children]
    };

    const getParentsFragments = (id) => {
        return [fragments[id].parents]
    };

    const getChildrenFragments = (id) => {
        return [fragments[id].children]
    };

    //DATASTRUCTURE: ici la construction de la structure de données.
    const buildDataReferences = ($dataRef) => {
        //ici, à partir de l'élément html qui contient la référence,
        //on construit un objet javascript qui ressemble à ça:
        //{
        //"0006":
        //    {
        //      "from":"Victoria", "title":"Turing Complete User", "author":"Olia Lialina", "loc":"p3"
        //    }
        //}

        //on divise le contenu texte en lignes
        const lines = $dataRef.innerHTML.split('\n');
        
        //on définit une pattern regex qui correspond à une ligne de référence
        const refRegex = /^f-\d{4} ?- ?.*$/;
        //on boucle sur les lignes
        lines.forEach(line => {

            //on vérifie si la ligne ressemble à une ligne de référence. 
            if(!refRegex.test(line))
                return;
            
            const parts = line.split('-');
            //en théorie parts = [f, id, prénom, référence]
            if(parts.length < 4)
                return;
            
            const reference = {};

            const id = parts[1].trim();
            reference['id'] = id;
            
            reference['from'] = parts[2].trim();
            let detail = parts[3].trim();

            if(parts.length > 4){
                detail = parts.slice(3).join(' ');
            }

            const refParts = detail.split(',');

            reference['title'] = refParts[0].trim();

            if(refParts.length > 1)
                reference['author'] = refParts[1].trim();
            if(refParts.length > 2)
                reference['loc'] = refParts[2].trim();

            references[id] = reference;
        });


    };


    const buildDataFragments = ($dataFragments) => {
        //ici, à partir de l'élément html qui contient les fragments,
        //on construit un objet javascript qui ressemble à ça:
        //{
        //"0006":
        //     {
        //         "html":"...", "type":"reponse", "parents":["0003"], "children":["0004"]
        //     },
        //"0004":{
        //         "html":"...", "type":"reponse", "parents:["0006", "0003"], "children":["0014"]}
        //}


        $dataFragments.innerHTML = mdToHtml($dataFragments.textContent);
        
        //chaque fragment est dans un <p class="fragment">
        const $fragments = $dataFragments.querySelectorAll('.fragment');
        //pour chacun
        $fragments.forEach($fragment => {
            //on déclare l'objet fragment 
            const fragment = {'children':[]};
        
            fragment['html'] = $fragment.innerHTML;
        
            const classes = $fragment.classList;
        
            const id = classes[1].replace('f-', '');
            fragment['id'] = id;

            if(classes.contains('reponse')){
                fragment['type'] = 'reponse';
            }else{
                fragment['type'] = 'start';
            }
        
            const parents = [];
            //boucle qui part de l'index 3 à l'index longueur de la liste - 1
            for(let index = 3; index < classes.length; index++){
                const parentId = classes[index].replace('r-', '');
                parents.push(parentId);
        
                //le fragment enfant se renseigne à ses parents en ajoutant son id 
                //au tableau children de chaque parent (ça ne marche que si les enfants sont encodés après les parents)
                if(fragments[parentId])
                    fragments[parentId]['children'].push(id);
        
            }
        
            fragment['parents'] = parents;
        
        
            //on ajoute l'objet fragment à l'objet fragments
            fragments[id] = fragment;
        });
        

        
    }





    buildDataFragments($dataFragments);

    buildDataReferences($dataRef);



    //RETOUR: on liste ici toutes les fonctions qui seront accessibles pour notre script principal
    return {fragments, getFragment, getRandomFragment, getReference, getLinkedFragments, getParentsFragments, getChildrenFragments};




    
};

export default dataBuilder;
