import dataBuilder from "./databuilder.js";
import eventsManager from "./eventsmanager.js";


const request = async(requestUrl, postData = {}) => {
        
    const response = await fetch(requestUrl, {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
      body: JSON.stringify(postData) // body data type must match "Content-Type" header*/
    });

    return response.json();

}

const $main = document.querySelector('main');
//on récupère l'élément html qui contient le texte brut des pads fragments
const $dataFragments = $main.querySelector('#fragments');
//on récupère l'élément html qui contient le texte brut du pad index
const $dataIndex = $main.querySelector('#index');

//on construit un objet "data" à partir de ces deux éléments html
//cet objet contient les données structurées et différentes fonctions utiles pour y accéder
const data = dataBuilder($dataFragments, $dataIndex);
const events = eventsManager($main);

//les données sont chargées; on vide la page de son contenu 
$main.innerHTML = '';

// si l'url de la page contient un hashtag (ce cas de figure intervient après un click sur un parent ou un enfant d'un fragment)
if(window.location.hash){
    // on récupère l'id du hashtag
    const idFragment = window.location.hash.split('#')[1];
    const fragment = data.getFragment(idFragment);
    // on appel la fonction contentPage pour faire apparaître le contenu de la page selon le fragment en question 
    contentPage(fragment, idFragment);
}
// si l'url de la page ne contient pas de hashtag (ce cas de figure intervient lors de la première entrée sur le site) 
else {
    // on récupère un fragment aléatoirement
    const fragment = data.getRandomFragment();
    const idFragment = fragment.id;
    // on appel la fonction contentPage pour faire apparaître le contenu de la page selon le fragment en question 
    contentPage(fragment, idFragment);
}

// var navigation = sessionStorage.getItem("nav")
// console.log(navigation)

// var xhr = new XMLHttpRequest(); 
// xhr.open('GET', 'http://localhost/2023-2024/erg/design-numerique/cadavre-exquis/natalia-louka-mondher/html-frag-parent-child/data.php#0010');
// xhr.onreadystatechange = function() {
//   if (xhr.readyState === 4) {
//     alert(xhr.responseText);
//   }
// };
// xhr.send();




function contentPage(frag, idFrag){
    // on créait différentes variables pour récupèrer le contenu du fragment et ces ids parents et children associés
    const contentFragment = frag.html;

    sessionStorage.setItem("nav", contentFragment);

    const idParents = data.getParentsFragments(idFrag)[0];
    const idChildren = data.getChildrenFragments(idFrag)[0];

    // on commence à structurer notre page HTML
    const article = document.createElement("article");
    $main.appendChild(article);

    // si le fragment à un ou plusieurs parent(s) alors on le(s) affiche dans la page
    // COMMENT VERIFIER L'ARRAY LENGHT SI IL EST NULL OU PAS
    if (idParents){
        const sectionParents = document.createElement("section");
        sectionParents.classList.add("parents");
        article.appendChild(sectionParents);
        idParents.forEach(idParent => {
            const divParent = document.createElement("div");
            divParent.classList.add("parent");
            divParent.setAttribute('data-id', idParent);
            sectionParents.appendChild(divParent);
            const contentParent = data.getFragment(idParent).html;
            divParent.innerHTML = contentParent;
        });
    }

    // on intègre le fragment dans la page
    const section = document.createElement("section");
    const div = document.createElement("div");
    section.classList.add("fragment");
    div.setAttribute('data-id', idFrag);
    article.appendChild(section);
    section.appendChild(div);
    div.innerHTML = contentFragment;

    // si le fragment à un ou plusieurs enfant(s) alors on le(s) affiche dans la page
    if (idChildren){
        const sectionChildren = document.createElement("section");
        sectionChildren.classList.add("children");
        article.appendChild(sectionChildren);
        idChildren.forEach(idChild => {
            const divChild = document.createElement("div");
            divChild.classList.add("child");
            divChild.setAttribute('data-id', idChild);
            sectionChildren.appendChild(divChild);
            const contentChild = data.getFragment(idChild).html;
            divChild.innerHTML = contentChild;
        });
    }

    // si je click sur un parent alors il devient fragment
    const parents = $main.querySelectorAll(".parent");
    if (parents){
        parents.forEach(parent => {
            parent.addEventListener("click", function(){
                var newId = parent.getAttribute("data-id");
                // on ajoute une ancre à notre page web en précisant l'id du nouveau fragment
                window.location.hash = newId;
                // on refresh la page
                window.location.reload(true);
            }); 
        });
    }

    // si je click sur un child alors il devient fragment
    const children = $main.querySelectorAll(".child");
    if (children){
        children.forEach(child => {
            child.addEventListener("click", function(){
                var newId = child.getAttribute("data-id");
                // on ajoute une ancre à notre page web en précisant l'id du nouveau fragment
                window.location.hash = newId;
                // on refresh la page
                window.location.reload(true);
            }); 
        });
    }
}