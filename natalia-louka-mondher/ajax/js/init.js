import dataBuilder from "./databuilder.js";
// import eventsManager from "./eventsmanager.js";

//// HISTORY ////
let history = [];
const request = async(requestUrl, postData = {}) => {
    const response = await fetch(requestUrl, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(postData) // body data type must match "Content-Type" header*/
    });
    return response.json();
}

//// CONSTRUCTION ////
const $main = document.querySelector('main');
//on récupère l'élément html qui contient le texte brut des pads fragments
const $dataFragments = $main.querySelector('#fragments');
//on récupère l'élément html qui contient le texte brut du pad index
const $dataIndex = $main.querySelector('#index');

//on construit un objet "data" à partir de ces deux éléments html
//cet objet contient les données structurées et différentes fonctions utiles pour y accéder
const data = dataBuilder($dataFragments, $dataIndex);
// const events = eventsManager($main);

//les données sont chargées; on vide la page de son contenu 
$main.innerHTML = '';

// si l'url de la page contient un hashtag (ce cas de figure intervient après un click sur un parent ou un enfant d'un fragment)
if(window.location.hash){
    // on récupère l'id du hashtag
    // const time = window.location.hash.split('?')[1];
    // const id = window.location.hash.split('?')[0];
    // const idFragment = id.split('#')[1];
    const idFragment = window.location.hash.split('#')[1];
    const fragment = data.getFragment(idFragment);
    // on appel la fonction contentPage pour faire apparaître le contenu de la page selon le fragment en question 
    contentPage(fragment, idFragment, false);
}
// si l'url de la page ne contient pas de hashtag (ce cas de figure intervient lors de la première entrée sur le site) 
else{
    // on récupère un fragment aléatoirement
    // const time = '';
    const fragment = data.getRandomFragment();
    const idFragment = fragment.id;
    // on appel la fonction contentPage pour faire apparaître le contenu de la page selon le fragment en question 
    contentPage(fragment, idFragment, true);

}

function contentPage(frag, idFrag, start){
    let currentTime = new Date();
    // on créait différentes variables pour récupèrer le contenu du fragment et ces ids parents et children associés
    const contentFragment = frag.html;
    const idParents = data.getParentsFragments(idFrag)[0];
    const idChildren = data.getChildrenFragments(idFrag)[0];

    // on commence à structurer notre page HTML
    const article = document.createElement("article");
    $main.appendChild(article);

    // si le fragment à un ou plusieurs parent(s) alors on le(s) affiche dans la page
    // COMMENT VERIFIER L'ARRAY LENGHT SI IL EST NULL OU PAS
    if (idParents){
        const sectionParents = document.createElement("section");
        sectionParents.classList.add("parents");
        article.appendChild(sectionParents);
        idParents.forEach(idParent => {
            const divParent = document.createElement("div");
            divParent.classList.add("parent");
            divParent.setAttribute('data-id', idParent);
            const contentParent = data.getFragment(idParent).html;
            divParent.innerHTML = contentParent;
            const refParent = data.getReference(idParent);
            const authorParent = refParent.author;
            const spanParent = '<span class="author">' + '@' + authorParent + ': ' + '</span>'
            divParent.insertAdjacentHTML('afterbegin', spanParent);
            sectionParents.appendChild(divParent);
        });
    }

    // on intègre le fragment dans la page
    const section = document.createElement("section");
    const div = document.createElement("div");
    section.classList.add("fragment");
    div.setAttribute('data-id', idFrag);
    article.appendChild(section);
    div.innerHTML = contentFragment;
    const ref = data.getReference(idFrag);
    const author = ref.author;
    const span = '<span class="author">' + '@' + author + ': ' + '</span>'
    div.insertAdjacentHTML('afterbegin', span);
    section.appendChild(div);

    // si le fragment à un ou plusieurs enfant(s) alors on le(s) affiche dans la page
    if (idChildren){
        const sectionChildren = document.createElement("section");
        sectionChildren.classList.add("children");
        article.appendChild(sectionChildren);
        idChildren.forEach(idChild => {
            const divChild = document.createElement("div");
            divChild.classList.add("child");
            divChild.setAttribute('data-id', idChild);
            const contentChild = data.getFragment(idChild).html;
            divChild.innerHTML = contentChild;
            const refChild = data.getReference(idChild);
            const authorChild = refChild.author;
            const spanChild = '<span class="author">' + '@' + authorChild + ': ' + '</span>'
            divChild.insertAdjacentHTML('afterbegin', spanChild);
            sectionChildren.appendChild(divChild);
        });
    }

    const sendId = async(id, start) => {
        const reponse = await request(window.location, {'id': id, 'start':(start)?1:0});
        history = reponse;
    };    

    sendId(idFrag, start);


    // si je click sur un parent alors il devient fragment
    const parents = $main.querySelectorAll(".parent");
    if (parents){
        parents.forEach(parent => {
            parent.addEventListener("click", function(){
                const newId = parent.getAttribute("data-id");
                // on ajoute une ancre à notre page web en précisant l'id du nouveau fragment
                window.location.hash = newId;
                // window.location.hash = newId + '?' + currentTime.toLocaleTimeString();
                // on refresh la page
                window.location.reload(true);
            }); 
        });
    }

    // si je click sur un child alors il devient fragment
    const children = $main.querySelectorAll(".child");
    if (children){
        children.forEach(child => {
            child.addEventListener("click", function(){
                console.log(currentTime);
                const newId = child.getAttribute("data-id");
                // on ajoute une ancre à notre page web en précisant l'id du nouveau fragment
                window.location.hash = newId
                // window.location.hash = newId + '?' + currentTime.toLocaleTimeString();
                // on refresh la page
                window.location.reload(true);
            }); 
        });
    }
}


//////////// REFRESH ///////////////
const $btnRefresh = document.querySelector('button.refresh');
$btnRefresh.addEventListener('click', (e) => {
    window.location.href.split('#')[0]
    window.location.reload(true);
});

//////////// PRINT ///////////////
const $btnPrint = document.querySelector('button.print');
$btnPrint.addEventListener('click', (e) => {
    history.forEach(idHistory => {
        const historyFragment = data.getFragment(idHistory);
        const contentHistory = historyFragment.html;
        const refHistory = data.getReference(idHistory);
        const userHistory = refHistory.from;
        const authorHistory = refHistory.author;
        const $printDoc = document.querySelector("iframe").contentDocument;
        const $mainPrint = $printDoc.querySelector('main');
        const $articlePrint = $printDoc.createElement("article");
        // const $divAuthor = document.createElement("div");
        const $divContent = document.createElement("div");
        // const $divDate = document.createElement("div");
        const $authorHistory = '<span class="author">' + '@' + authorHistory + ': ' + '</span>'
        // $divAuthor.innerHTML = 
        $divContent.innerHTML = contentHistory;
        $divContent.insertAdjacentHTML('afterbegin', $authorHistory);
        $articlePrint.classList.add("history");
        $articlePrint.appendChild($divContent);
        $mainPrint.appendChild($articlePrint);
    });

    window.frames['frame'].print(); 
});


// let currentTime = new Date();
// console.log(currentTime);