<?php
session_start();
$content = trim(file_get_contents("php://input"));

if($content != ''){
    $content = json_decode($content);

    if(isset($content->id)){
        // si (is set) si il est définit alors
        if($content->start == 0){
            $_SESSION['historique'][] = $content->id;
        } else {
            //session_destroy();
            $_SESSION['historique'] = [];
            $_SESSION['historique'][] = $content->id;
        }
        echo json_encode($_SESSION['historique']);
    }
}else{
$dataFragments = '';
//ça c'est pour charger les trois pads d'un coup. S'il y en avait 4 ben on changerait le 3 en 4.
for($i = 1; $i <= 3; $i++){
    $dataFragments .= file_get_contents('https://pads.erg.be/p/dnum-frags-'.sprintf("%'.03d", $i).'/export/txt');
}
$dataIndex = file_get_contents('https://pads.erg.be/p/dnum-frags-ids/export/txt');
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <script type="module" defer src="js/init.js"></script>
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>

    <main>
        <section id="fragments">
            <?php echo $dataFragments; ?>
        </section>
        <section id="index">
            <?php echo $dataIndex; ?>
        </section>
    </main>

<button class="refresh">Refresh</button>
<iframe src="print.php" name="frame"></iframe>
<button class="print">Print</button>
</body>
</html>

<?php 
}
// session_destroy();
?>