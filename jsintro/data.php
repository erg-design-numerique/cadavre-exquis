
<?php

//https://pads.erg.be/p/dnum-frags-001/export/txt
//https://pads.erg.be/p/dnum-frags-002/export/txt
//https://pads.erg.be/p/dnum-frags-003/export/txt

$data1 = file_get_contents("https://pads.erg.be/p/dnum-frags-001/export/txt");
$data3 = file_get_contents("https://pads.erg.be/p/dnum-frags-003/export/txt");

?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <script type="module" defer src="js/init.js"></script>
    <link rel="stylesheet" href="styles.css">
    
</head>
<body><?php echo $data1.$data3; ?></body>
</html>
