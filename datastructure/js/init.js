import mdToHtml from "./mdconverter.js";
const $main = document.querySelector('main');
const mdData = $main.textContent;
const htmlData = mdToHtml(mdData)

$main.innerHTML = htmlData;

const $fragments = document.querySelectorAll('.fragment');
const fragments = {};

$fragments.forEach($fragment => {
    //on déclare l'objet fragment 
    const fragment = {'children':[]};
    fragment['html'] = $fragment.innerHTML;
    const classes = $fragment.classList;
    const id = classes[1].replace('f-', '');
    if(classes.contains('reponse')){
        fragment['type'] = 'reponse';
    }else{
        fragment['type'] = 'start';
    }

    const parents = [];
    //boucle qui part de l'index 3 à l'index longueur de la liste - 1
    for(let index = 3; index < classes.length; index++){
        const parentId = classes[index].replace('r-', '');
        parents.push(parentId);
        //le fragment enfant se renseigne à ses parents en ajoutant son id 
        //au tableau children de chaque parent (ça ne marche que si les enfants sont encodés après les parents)
        if(fragments[parentId])
            fragments[parentId]['children'].push(id);
    }

    fragment['parents'] = parents;
    //on ajoute l'objet fragment à l'objet fragments
    fragments[id] = fragment;
});

$main.innerHTML = '';

// Fonction qui permet d'afficher un fragment au hasard
function randomFragment(){
    const keys = Object.keys(fragments);
    var key = keys[Math.floor(Math.random() * keys.length)];
    const div = document.createElement("div");
    document.body.appendChild(div);
    div.innerHTML = fragments[key]['html'];
}

randomFragment()

$main.style.visibility = "visible";