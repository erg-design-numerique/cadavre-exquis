/*
We want to translate the information that the HTML holds into something readable and usable in JavaScript
'init' means initialise

We loop through all the fragments, get the list of their classes, and from there store the information about their parents and children.
sorry4notranslation
*/

const $fragments = document.querySelectorAll('.fragment'); // Dollar sign to distinguish that the variable is a HTML element

const fragments = {}; // init list of fragments

$fragments.forEach($fragment => { // loop through fragment elements
    const fragment = {'html': $fragment.innerHTML}; // initialise fragment not as element but as an associative array
    // It's like an object with properties that JS can read easier

    const classes = $fragment.classList; // put the element's classes in a list

    const id = classes[1].replace('f-',''); // assign the id as a number without the f-


    if(classes.contains('reponse')){ // If reponse is in the classes
        fragment['type']='reponse'; // the fragment type is a reponse
    } else { // or
        fragment['type']='start'; // if not, the type is a starting fragment
    }

    fragment['children'] = []; // init children variable

    const parentslist = [];  // create a variable for the list of parents
    for(let index = 3; index < classes.length; index++){ // start loop at index 3 because that's the index parents start at; and for each:
        const parentId = classes[index].replace('r-',''); // make it digits only, no r-
        parentslist.push(parentId); // push it to the list of parents of the object


        if(fragments[parentId] != null){ // if the referenced parent exists
            fragments[parentId]['children'].push(id); // inform the parent that it has a child
            // Basically In the list of fragments, 
            // the item with the ID corresponding to a Parent ID of this item, 
            // gets appended this item's ID in its list of children
            // Basically child tells parent it is the latter's child. AS GOD INTENDED?

        }
        
    }
    fragment['parents'] = parentslist; //put the list of parents in the fragment's properties

    //console.log(parentslist);
    
    fragments[id]= fragment; // put the fragment in the fragments associative array, according to its number

    


    // on ajoute l'objet fragment singulier à l'objet fragments pluriel
    
});

console.log(fragments); // log em all
