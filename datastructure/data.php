<?php

//https://pads.erg.be/p/dnum-frags-001/export/txt
//https://pads.erg.be/p/dnum-frags-002/export/txt
//https://pads.erg.be/p/dnum-frags-003/export/txt

//ça c'est pour charger les trois pads d'un coup. S'il y en avait 4 ben on changerait le 3 en 4.
$data = '';
for($i = 1; $i <= 3; $i++){
    $data .= file_get_contents('https://pads.erg.be/p/dnum-frags-'.sprintf("%'.03d", $i).'/export/txt');
}
?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <script type="module" defer src="js/init.js"></script>
    <link rel="stylesheet" href="styles.css">
</head>
<body><main><?php echo $data; ?></main></body>
</html>
