import mdToHtml from "./mdconverter.js";


const $main = document.querySelector('main');
const mdData = $main.textContent;


const htmlData = mdToHtml(mdData)

$main.innerHTML = htmlData;

window.PagedPolyfill.preview();

