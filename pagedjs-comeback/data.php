
<?php

//https://pads.erg.be/p/dnum-frags-001/export/txt
//https://pads.erg.be/p/dnum-frags-002/export/txt
//https://pads.erg.be/p/dnum-frags-003/export/txt

$data1 = file_get_contents("https://pads.erg.be/p/dnum-frags-001/export/txt");
$data3 = file_get_contents("https://pads.erg.be/p/dnum-frags-003/export/txt");

?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <script>
        window.PagedConfig = {
            auto: false,
            after: (flow) => { console.log("after", flow) },
	    };
    </script>
    <script src="js/libs/paged.polyfill.min.js"></script>
    <script type="module" defer src="js/init.js"></script>
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/pagedjs.css">
    
</head>
<body>
    <main><?php echo $data1.$data3; ?></main>
</body>
</html>
