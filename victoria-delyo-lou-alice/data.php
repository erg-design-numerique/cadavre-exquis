<?php

//https://pads.erg.be/p/dnum-frags-001/export/txt
//https://pads.erg.be/p/dnum-frags-002/export/txt
//https://pads.erg.be/p/dnum-frags-003/export/txt


$dataFragments = '';
//ça c'est pour charger les trois pads d'un coup. S'il y en avait 4 ben on changerait le 3 en 4.
for($i = 1; $i <= 3; $i++){
    $dataFragments .= file_get_contents('https://pads.erg.be/p/dnum-frags-'.sprintf("%'.03d", $i).'/export/txt');
}

$dataIndex = file_get_contents('https://pads.erg.be/p/dnum-frags-ids/export/txt');

?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <script type="module" defer src="js/init.js"></script>
    <script src="js/interface.js"></script>
  <script src="js/imposition.js"></script>
    <link rel="stylesheet" href="css/styles.css">

</head>
<body>
<header>Coucou voici la page titre</header>
    <main>
        <section id="fragments">
            <?php echo $dataFragments; ?>
        </section>
        <section id="index">
            <?php echo $dataIndex; ?>
        </section>
    </main>
<footer>page de fin</footer>

</body>
</html>
