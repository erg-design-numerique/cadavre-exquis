import dataBuilder from "./databuilder.js";
import eventsManager from "./eventsmanager.js";
import {
    Previewer
} from "./libs/paged.esm.js";


// OUTILS



function displayFragment(fragment) {

    $main.innerHTML = '';

    historique.push(fragment);


    const contentParent = fragment.html;
    const idFragment = fragment.id;
    const sectionMorceau = document.createElement("section");
    const ghostBody = document.createElement("section");
    const divParent = document.createElement("div");
    const pParent = document.createElement('p');

  
    ghostBody.classList.add("darkBody");
    sectionMorceau.classList.add("morceau");
    divParent.setAttribute('id', 'parent');
    divParent.setAttribute('data-id', idFragment);
    $main.appendChild(ghostBody);
    $main.appendChild(sectionMorceau);
    sectionMorceau.appendChild(divParent);
    divParent.appendChild(pParent);
    pParent.innerHTML = contentParent;

   


    const idEnfants = data.getChildrenFragments(idFragment);
    console.log(idEnfants);

    const divEnfants = document.createElement("div");
    divEnfants.classList.add("enfants");
    sectionMorceau.appendChild(divEnfants);
    idEnfants.forEach(idEnfant => {
        const imgpath = 'img/' + idEnfant + '.png';
        const imgEnfant = document.createElement("img");
        imgEnfant.classList.add("enfant");
        imgEnfant.setAttribute('data-id', idEnfant);
        imgEnfant.setAttribute('src', imgpath);
        divEnfants.appendChild(imgEnfant);
        divEnfants.appendChild(imgEnfant);
  
        

        // si il n'y a pas d'image qui correspond au idEnfant, afficher l'image 00

        // imgEnfant.innerHTML = contentEnfant;
        // const pEnfant = document.createElement("p");
        // imgEnfant.appendChild(pEnfant);
        // pEnfant.innerHTML = contentEnfant;
    });


    AccentColor();
    random();
    background();
    resize();

    const enfants = $main.querySelectorAll(".enfant");
    //quand il y a plus d'enfant : il y a un bouton qui dit "imprimer" 
    if (enfants.length == 0) {
        const divFin = document.createElement("span");
        divFin.classList.add("fin");
        const btn = document.createElement("button");
        btn.innerHTML = "imprimer!!!!";
        sectionMorceau.appendChild(divFin);
        divFin.appendChild(btn);
        btn.addEventListener("click", imprimer);

    }



    enfants.forEach(enfant => {
        enfant.addEventListener("click", function () {

            displayFragment(data.getFragment(enfant.getAttribute("data-id")));
        });

        enfant.addEventListener("mouseover", function () {
            document.getElementById("parent").style.filter = "blur(2px)";
        });

        enfant.addEventListener("mouseout", function () {
            document.getElementById("parent").style.filter = "blur(0px)";


        });


    });

    function random() {


        // images
        let spans = document.querySelectorAll('.enfant');
        spans.forEach(span => {
            let top = Math.random() * window.innerHeight / 2;
            let left = Math.random() * window.innerWidth / 2;
            span.style.top = `${top}px`;
            span.style.left = `${left}px`;

        });

        // fonts paragraphe
        var fontParagraphe = ["berolina", "dse", "routedgothic", "MetaAccanthisAlternate", "Redaction35"];
        var num;
        num = Math.floor(Math.random() * 5);
        document.getElementById("parent").style.fontFamily = fontParagraphe[num];

        // fonts lettrine
        var fontLettrine = ["Louise", "Goozee", "KaeruKaeru", "Tiny", "Noloice", "crozet", "BianZhiDai"];
        var num;
        num = Math.floor(Math.random() * 7);
        $main.style.setProperty('--accentFont', fontLettrine[num]);


        // var darkBackground = ["var(--rose)", "var(--blue)", "red",  "brown"];
        //   var num;
        //   num = Math.floor(Math.random() * 4);
        //   let body = document.querySelector('body');
        //   body.style.setProperty('--color', darkBackground[num]);
    }

    // let body = document.querySelector('body');
    // let couleurBody = body.style.backgroundColor;
    // body.style.backgroundColor = "rgb(201, 81, 39)";

    // function couleurComplementaire(couleur) {
    //     let r = 255 - parseInt(couleur.substring(1, 3), 16);
    //     let g = 255 - parseInt(couleur.substring(3, 5), 16);
    //     let b = 255 - parseInt(couleur.substring(5, 7), 16);
    //     return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1).toUpperCase();
    //  }

    // let quote = document.querySelector('#parent p');

    //  let couleur_complementaire = couleurComplementaire(couleurBody);
    // quote.style.color = couleur_complementaire;


}

function AccentColor() {
    var randomAccentColor = ["red", "var(--rose)", "var(--blue)", "var(--yellow)", "var(--green)"];
    var num;
    num = Math.floor(Math.random() * 5);
    let morceau = document.querySelector('.morceau');
    morceau.style.setProperty('--accentColor', randomAccentColor[num])
}

function background() {

    let opacity = 0;
    var ghostBody = document.querySelector('.darkBody');
    opacity += 0.5;
    let fonds = document.querySelectorAll('.enfant');
    fonds.forEach(fond => {
        ghostBody.style.display = "block";
        ghostBody.style.opacity = opacity;
    });
}

function resize() {
    var p = document.querySelector('#parent p');
    var lineHeight = parseInt(window.getComputedStyle(p).fontSize);
    var lines = Math.floor(p.offsetHeight / lineHeight);
    if (lines < 7) {
        p.style.fontSize = '50px';
        let fontSize = "200px";
        $main.style.setProperty('--accentFontSize', fontSize);
    } else(lines > 7); {
        let fontSize = "100px";
        $main.style.setProperty('--accentFontSize', fontSize);
    }
}


function imprimer() {
    $main.innerHTML = '';
    const cover = document.createElement("section");
    cover.classList.add("cover");
    const h1 = document.createElement("h1");
    const divImg = document.createElement("div");
    divImg.classList.add("image");

    
        var images = [
          './img/0029.png', 
          './img/0032.png',
          './img/0041.png',
        ];
        var listeImages = Math.floor(Math.random() * images.length);
        var img = document.createElement('img');
        img.src = images[listeImages];
       
    


  
    
    // font du titre 1 sur la couverture
    var fontType = ["berolina", "dse", "routedgothic", "MetaAccanthisAlternate", "Goozee", "Kaeru Kaeru", "Tiny", "Noloice", "Redaction 35"];
    var num;
    num = Math.floor(Math.random() * 9);
    $main.style.setProperty('--accentFont', fontType[num]);



    const textNode = document.createTextNode("À plusieurs mains");
    $main.appendChild(cover);
    cover.appendChild(h1);
    cover.appendChild(divImg);
    divImg.appendChild(img);
    h1.appendChild(textNode);

   
    // couleur du text shadow et du boxshadow de l'image et de la Lettrine
    var randomColor = ["var(--pink)", "var(--yellow)", "var(--green)", "aquamarine", "var(--purple)"];
    var num;
    num = Math.floor(Math.random() * 5);
    $main.style.setProperty('--accentColor', randomColor[num]);

    var randomColorParagraphe = ["var(--blue)", "brown", "red", "var(--rose)", "var(--sapin)", "var(--marine)"];
    var num;
    num = Math.floor(Math.random() * 6);
    $main.style.setProperty('--color', randomColorParagraphe[num]);


const index = [];

    historique.forEach(fragment => {
        console.log(fragment);
        const idFragment = fragment.id;
        

        const auteuriceFragment = data.getReference(idFragment).author;
        const cueilleureuseFragment = data.getReference(idFragment).from;
        // const references = [auteuriceFragment, cueilleureuseFragment];
        const sectionMorceau = document.createElement("section");
        const divFragment = document.createElement("div");
        const divImg = document.createElement("div");
        divImg.classList.add("image");

        const imgFragment = document.createElement("img");
        imgFragment.classList.add("imgFragment");
        imgFragment.setAttribute('data-id', idFragment);
        imgFragment.setAttribute('src', 'img/' + idFragment + '.png');
        const pMorceau = document.createElement('p');
        pMorceau.setAttribute('id', 'pFragment');


        sectionMorceau.classList.add("fragment");
        divFragment.setAttribute('id', 'fragment');

        $main.appendChild(sectionMorceau);
        sectionMorceau.appendChild(divFragment);
        divFragment.appendChild(divImg);
        divImg.appendChild(imgFragment);
        divFragment.appendChild(pMorceau);
        pMorceau.innerHTML = fragment.html;
      
        // fonts paragraphe
        var fontParagraphe = ["berolina", "dse", "routedgothic", "Redaction35"];
        var num;
        num = Math.floor(Math.random() * 4);
        pMorceau.style.fontFamily = fontParagraphe[num];

        // fonts lettrine
        var fontLettrine = ["Louise", "Goozee", "KaeruKaeru", "Tiny", "Noloice", "crozet", "BianZhiDai"];
        var num;
        num = Math.floor(Math.random() * 7);
        $main.style.setProperty('--accentFont', fontLettrine[num]);
     

        index.push(auteuriceFragment, cueilleureuseFragment);
    // console.log(index);

    });

    

    const backcover = document.createElement("section");
    backcover.classList.add("back-cover");
     
    const colophon = document.createElement("div");
    const pColophon = document.createElement("h1");
    pColophon.setAttribute('id', 'titre_colophon');

    const colophonText = document.createTextNode("Ont contribué à l'édition: ");
    $main.appendChild(backcover);
    backcover.appendChild(colophon);
    colophon.appendChild(pColophon);
    pColophon.appendChild(colophonText);

    console.log(index);
    const ulCueilleureuses = document.createElement("ul");
    const team = ["Lou", "Alice", "Victoria", "Delyo"];
    const teamEnseignante = ["Lionel", "Alexia", "Amélie"];


    for (let i = 0; i < index.length; i++) {
        let indexListe = document.createElement("li");
        indexListe.textContent = index[i];
        ulCueilleureuses.appendChild(indexListe);
     }

     for (let i = 0; i < team.length; i++) {
        let teamListe = document.createElement("li");
        teamListe.textContent = team[i];
        ulCueilleureuses.appendChild(teamListe);
     }

      for (let i = 0; i < teamEnseignante.length; i++) {
        let teamEnseignanteListe = document.createElement("li");
        teamEnseignanteListe.textContent = teamEnseignante[i];
        ulCueilleureuses.appendChild(teamEnseignanteListe);
     }

    backcover.appendChild(ulCueilleureuses);



    const paged = new Previewer();
    const content = $main.innerHTML;
    $main.innerHTML = '';
    const flow = paged.preview(content, ["css/styles.css", "css/paged.css"], $main).then((flow) => {
        console.log("Rendered", flow.total, "pages.");
    });






}





// CODE PRINCIPAL

const $main = document.querySelector('main');
//on récupère l'élément html qui contient le texte brut des pads fragments
const $dataFragments = $main.querySelector('#fragments');
//on récupère l'élément html qui contient le texte brut du pad index
const $dataIndex = $main.querySelector('#index');


//on construit un objet "data" à partir de ces deux éléments html
//cet objet contient les données structurées et différentes fonctions utiles pour y accéder
const data = dataBuilder($dataFragments, $dataIndex);


const events = eventsManager($main);

//les données sont chargées; on vide la page de son contenu 
$main.innerHTML = '';

const fragment = data.getRandomFragment(data.fragmentsOrphelins);
const historique = [];

displayFragment(data.getRandomFragment(data.fragmentsOrphelins));
















// Tentative afficher les REFS

// const reference = data.getReference();
// const contentReference = reference;
// const div = document.createElement("div");

// div.setAttribute('id', "references");
// $main.appendChild(div);
// div.innerHTML = contentReference;
// console.log(contentReference);






// console.log(data.getRandomFragment(data.fragmentsOrphelins));
//faire appel aux fonctions liées à data → utiliser data.function
//TODO: voir readme




//afficher un fragement aléatoire dans les fragments orphelins 
//prendre un id de fragment aléatoire puis afficher son contenu







// console.log(data.getRandomFragment);
// $main.innerHTML = data.getFragment('0014').html;
// console.log(data.getReference('0014'));

// console.log(data.getLinkedFragments('0014'));