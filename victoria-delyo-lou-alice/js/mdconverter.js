import "./libs/markdown-it.js";
import "./libs/markdown-it-attrs.js";
import { bracketed_spans_plugin } from "./libs/markdown-it-bracketed-spans.js";


// Loading Markdownit and its plugins
let md = window.markdownit({html: true});
md.use(markdownItAttrs);
bracketed_spans_plugin(md);

export default function mdToHtml(text) {
    return md.render(text);
}