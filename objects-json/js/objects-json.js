import mdToHtml from "./mdconverter.js";

const mdData = document.body.textContent;


const htmlData = mdToHtml(mdData);

document.body.innerHTML = htmlData;


const fragments = document.querySelectorAll('.fragment.reponse');


fragments.forEach(fragment => {
    fragment.addEventListener('mouseenter', (event) => {
        fragment.classList.add('active');
        //console.log(fragment.innerHTML);
    });
});

console.log(fragments);



let tableau_fragments = [];

fragments.forEach(fragment => {
    let frag = { classList : fragment.classList, innerHTML: fragment.innerHTML };
    tableau_fragments.push(frag);
    
});

console.log(tableau_fragments);



const data_fragments = JSON.stringify(tableau_fragments, null, 2);

console.log(data_fragments);

/* 

let element = document.createElement('a');
element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(data_fragments));
element.setAttribute('download', data_fragments);

element.style.display = 'none';
document.body.appendChild(element);

element.click();

document.body.removeChild(element); */
