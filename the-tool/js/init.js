import dataBuilder from "./databuilder.js";
import eventsManager from "./eventsmanager.js";


const $main = document.querySelector('main');
//on récupère l'élément html qui contient le texte brut des pads fragments
const $dataFragments = $main.querySelector('#fragments');
//on récupère l'élément html qui contient le texte brut du pad index
const $dataIndex = $main.querySelector('#index');

//on construit un objet "data" à partir de ces deux éléments html
//cet objet contient les données structurées et différentes fonctions utiles pour y accéder
const data = dataBuilder($dataFragments, $dataIndex);


const events = eventsManager($main);

//les données sont chargées; on vide la page de son contenu 
$main.innerHTML = '';




console.log(data.fragments);
console.log(data.getRandomFragment());


$main.innerHTML = '<div>'+data.getFragment('0014').html+'</div>';
console.log(data.getReference('0014'));

console.log(data.getLinkedFragments('0014'));


if(document.querySelector('header') != null){
    $main.prepend(document.querySelector('header'));
}
if(document.querySelector('footer') != null){
    $main.append(document.querySelector('footer'));
}

